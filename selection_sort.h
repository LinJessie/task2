#ifndef SELECTION_SORT_H
#define SELECTION_SORT_H
using namespace std;


#include <functional>

namespace mylib {

  //selectionsort
  template <class RandomAccessIterator, class Compare = std::less<typename RandomAccessIterator::value_type>>
  void selection_sort ( RandomAccessIterator first, RandomAccessIterator last, Compare cmp = Compare() ) {

    using size_type = typename RandomAccessIterator::difference_type;
    size_type n {last - first};

    for( size_type i {0}; i < n; ++i ) {

      size_type j {i};
      size_type k {j+1};
      for( ; k < n; ++k )
        if( cmp(first[k],first[j]) ) j = k;

      if(i!=j)
    std::swap( first[i], first[j] );
    }

  }
  
  //heapsort
  template <class RandomAccessIterator, class Compare = std::less<typename RandomAccessIterator::value_type>>
  void heapsort( RandomAccessIterator first, RandomAccessIterator last, Compare comp = Compare() ) {  
      using size_type = typename RandomAccessIterator::difference_type;
      size_type n {last - first};
      for(size_type i {n/2}; i >= begin; i--)
         heapshiftdown(first,last,i,comp);
      for(size_type j{last};j>first;j--)
      {
          std::swap(first,j);
          heapshiftdown(j,first,first,comp);
      }         
  }
  template <class RandomAccessIterator, class Compare = std::less<typename RandomAccessIterator::value_type>>
  void heapshiftdown( RandomAccessIterator first, RandomAccessIterator last, RandomAccessIterator i, Compare comp = Compare() ) {  
      using size_type = typename RandomAccessIterator::difference_type;
      size_type largest;
      if(i-1 < last && comp(lagerst,i-1)) largest=i-1;
      if(i+1 < last && comp(lagerst,i+1)) largest=i+1;
      if(lagerst != i)
      {
          std::swap(largest,i);
          heapshiftdown(first,last,largest,comp);
      }    
  }
  
  //quicksort
  template <class RandomAccessIterator, class Compare = std::less<typename RandomAccessIterator::value_type>>
  void quicksort( RandomAccessIterator first, RandomAccessIterator last, Compare comp = Compare() ) {
      using size_type = typename RandomAccessIterator::difference_type;
      size_type mid{(last-first)/2};
      size_type n{last-first};
      for(size_type i {0};i<n/2;i++)
      {
          if(!comp(i,mid)) std::swap(i,mid);
          if(!comp(mid,last-i)) std::swap(last-i,mid);
          quicksort(first,n/2,comp);
      }
      for(size_type j {n/2};i<last;j++)
      {
          if(!comp(j,mid)) std::swap(j,mid);
          if(!comp(mid,last-j)) std::swap(last-j,mid);
          quicksort(n/2,last,comp);
      }
  }

  //median3
  template <class RandomAccessIterator, class Compare = std::less<typename RandomAccessIterator::value_type>>
  RandomAccessIterator median3( RandomAccessIterator first, RandomAccessIterator last, RandomAccessIterator mid, Compare comp = Compare() ) {
      if(comp(first,mid))
      {
          if(comp(mid,last))
              return mid;
          else if (comp(first,last))
              return last;
          else
              return first;
      }
      else if(comp(first,last))
          return first;
      else if (comp(mid,last))
          return last;
      else
          return mid;
  }

  //partition
  template <class RandomAccessIterator, class Compare = std::less<typename RandomAccessIterator::value_type>>
  RandomAccessIterator partition( RandomAccessIterator first, RandomAccessIterator last, RandomAccessIterator mid, Compare comp = Compare() ) {
      using size_type = typename RandomAccessIterator::difference_type;
      size_type index {first};
      std::swap(mid,last);
      size_type pivot {last};
      for(size_type i{first};i<last;i++)
      {
          if(comp(i,pivot))
              std::swap(index++,i);
      }
      std::swap(last,index);
      return index;
  }


  //introsort
  template <class RandomAccessIterator, class Compare = std::less<typename RandomAccessIterator::value_type>>
  void introsort( RandomAccessIterator first, RandomAccessIterator last, Compare comp = Compare() ) {
      using size_type = typename RandomAccessIterator::difference_type;
      size_type &threshold = 20;
      size_type depth{0};
      size_type n{last-first};
      for(;n>1;n>>=1) ++depth;
      if(last-first >1)
      {        
          while(last-first<threshold)
          {
              if(depth==0)
              {
                  heapsort(first,last,comp);
                  return;
              }
              else
                  quicksort(first,last,comp);
              --depth;
              size_type cut = partition(first,last,median3(first,last,(last-first)/2,comp),comp);
              introsort(cut,last,comp);
              last = {cut};
          }
          selection_sort(first,last,comp);
      }
  }
  
  
  
  
  




//--------------------------------------------------------
//      size_type m {last - first};
//      for m = 2:n;
//          for (k = i; k > 1 and a[k] < a[k-1]; k--) {
//              swap a[k,k-1];



//  void introSort(int array[],int len)
//  {
//          if(len!=1)
//          {
//                  introSortLoop(array,0,len-1,lg(len)*2);
//                  insertionSort(array,len);
//          }
//  }


}




#endif // SELECTION_SORT_H
